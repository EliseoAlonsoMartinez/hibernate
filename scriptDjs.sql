CREATE DATABASE IF NOT EXISTS DJS;

USE DJS;

CREATE TABLE IF NOT EXISTS DJS(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(50) NOT NULL,
categoria VARCHAR(20) NOT NULL,
fecha_contrato DATETIME,
sueldo FLOAT NOT NULL,
edad INT
);

CREATE TABLE IF NOT EXISTS evento(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(50) NOT NULL UNIQUE,
cantidad_eventos INT,
ciudad VARCHAR(50),
fecha_apertura DATETIME
);

CREATE TABLE IF NOT EXISTS cliente(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(20) NOT NULL,
dni VARCHAR(9) UNIQUE NOT NULL,
fecha_nacimiento DATETIME,
numero_cliente INT UNIQUE
);

CREATE TABLE IF NOT EXISTS entrada(
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
codigo VARCHAR(20) UNIQUE NOT NULL,
precio FLOAT NOT NULL,
fecha_caducidad DATETIME NOT NULL,

id_evento INT UNSIGNED,
INDEX (id_evento),
FOREIGN KEY (id_evento)
	REFERENCES evento(id),
	
id_cliente INT UNSIGNED,
INDEX (id_cliente),
FOREIGN KEY (id_cliente)
	REFERENCES cliente(id)

);


CREATE TABLE IF NOT EXISTS dj_evento(

id_DJS INT UNSIGNED,
INDEX (id_DJS),
FOREIGN KEY (id_DJS)
	REFERENCES DJS (id),
	
id_evento INT UNSIGNED PRIMARY KEY,
INDEX (id_evento),
FOREIGN KEY (id_evento)
	REFERENCES evento (id)
		ON UPDATE CASCADE ON DELETE CASCADE
) ;

CREATE TABLE IF NOT EXISTS evento_cliente(
id_cliente INT UNSIGNED PRIMARY KEY,
INDEX (id_cliente),
FOREIGN KEY (id_cliente)
	REFERENCES cliente (id),

id_evento INT UNSIGNED,
INDEX (id_evento),
FOREIGN KEY (id_evento)
	REFERENCES evento (id)
	ON UPDATE CASCADE ON DELETE CASCADE
);
