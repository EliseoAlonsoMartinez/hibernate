package org.eli.DJS.base;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Eliseo on 06/02/2016.
 */
@Entity
@Table(name="djs")
public class DJ {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="nombre")
    private String nombre;
    @Column(name="categoria")
    private String categoria;
    @Column(name="fecha_contrato")
    private Date fecha_contrato;
    @Column(name="sueldo")
    private float sueldo;
    @Column(name="edad")
    private int edad;

    @ManyToMany(mappedBy = "dj",cascade = CascadeType.DETACH)
    List<Evento> evento;

    public DJ(){
        evento =new ArrayList<Evento>();
    }

    public List<Evento> getEvento() {
        return evento;
    }

    public void setEvento(List<Evento> evento) {
        this.evento = evento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public Date getFecha_contrato() {
        return fecha_contrato;
    }

    public void setFecha_contrato(Date fecha_nacimiento) {
        this.fecha_contrato = fecha_nacimiento;
    }

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
