package org.eli.DJS.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Eliseo on 06/02/2016.
 */
@Entity
@Table(name="cliente")
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name="nombre")
    private String nombre;
    @Column(name="dni")
    private String dni;
    @Column(name="fecha_nacimiento")
    private Date fecha_nacimiento;
    @Column(name="numero_cliente")
    private int numero_cliente;


    @ManyToMany(mappedBy = "cliente",cascade = CascadeType.DETACH)
    List<Evento> evento;

    @OneToMany(mappedBy = "cliente",cascade = CascadeType.DETACH)
    List<Entrada> entrada;

    public Cliente(){
        evento =new ArrayList<Evento>();
        entrada =new ArrayList<Entrada>();
    }

    public List<Entrada> getEntrada() {
        return entrada;
    }

    public void setEntrada(List<Entrada> entrada) {
        this.entrada = entrada;
    }

    public List<Evento> getEvento() {
        return evento;
    }

    public void setEvento(List<Evento> evento) {
        this.evento = evento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String apellidos) {
        this.dni = apellidos;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public int getNumero_cliente() {
        return numero_cliente;
    }

    public void setNumero_cliente(int numero_cliente) {
        this.numero_cliente = numero_cliente;
    }

    @Override
    public String toString() {
        return nombre + "   " + numero_cliente;
    }
}
