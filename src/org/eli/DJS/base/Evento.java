package org.eli.DJS.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Eliseo on 06/02/2016.
 */
@Entity
@Table(name="evento")
public class Evento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="nombre")
    private String nombre;
    @Column(name="cantidad_eventos")
    private int cantidad_fiestas;
    @Column(name="ciudad")
    private String ciudad;
    @Column(name="fecha_apertura")
    private Date fecha_apertura;


    @OneToMany(mappedBy = "evento",cascade = CascadeType.DETACH)
    private List<Entrada> entrada;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name="dj_evento",
            joinColumns={@JoinColumn(name="id_evento")},//Este es el id de la propia clase en �ste caso
            inverseJoinColumns={@JoinColumn(name="id_djs")})
    List<DJ> dj;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name="evento_cliente",
              joinColumns={@JoinColumn(name="id_evento")},
               inverseJoinColumns = {@JoinColumn(name="id_cliente")})

    List<Cliente> cliente;


    public Evento(){
        dj =new ArrayList<DJ>();
        cliente =new ArrayList<Cliente>();
        entrada =new ArrayList<Entrada>();
    }



    public List<Entrada> getEntrada() {
        return entrada;
    }

    public void setEntrada(List<Entrada> entrada) {
        this.entrada = entrada;
    }

    public List<DJ> getDj() {
        return dj;
    }

    public void setDj(List<DJ> dj) {
        this.dj = dj;
    }

    public List<Cliente> getListaCliente() {
        return cliente;
    }

    public void setListaCliente(List<Cliente> listaCliente) {
        this.cliente = listaCliente;
    }

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getNombre() {return nombre;}

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidad_fiestas() {
        return cantidad_fiestas;
    }

    public void setCantidad_fiestas(int cantidad_fiestas) {
        this.cantidad_fiestas = cantidad_fiestas;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Date getFecha_apertura() {
        return fecha_apertura;
    }

    public void setFecha_apertura(Date fecha_apertura) {
        this.fecha_apertura = fecha_apertura;
    }

    @Override
    public String toString() {
        return nombre +ciudad;
    }
}
