package org.eli.DJS.base;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Eliseo on 06/02/2016.
 */
@Entity
@Table(name="entrada")
public class Entrada {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name="codigo")
    private String codigo;
    @Column(name="precio")
    private float precio;
    @Column(name="fecha_caducidad")
    private Date fecha_caducidad;



    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name="id_cliente")
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name="id_evento")
    private Evento evento;

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public Date getFecha_caducidad() {
        return fecha_caducidad;
    }

    public void setFecha_caducidad(Date fecha_caducidad) {
        this.fecha_caducidad = fecha_caducidad;
    }

    @Override
    public String toString() {
        return codigo+ "    "+fecha_caducidad;
    }
}
