package org.eli.DJS.gui;


import org.eli.DJS.base.Cliente;
import org.eli.DJS.base.DJ;
import org.eli.DJS.base.Entrada;
import org.eli.DJS.base.Evento;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Eliseo on 12/02/2016.
 */
public class VentanaController {

    private Ventana ventana;
    private VentanaModel model;

    private DJ dj=null;
    private Evento evento;
    private Cliente cliente;
    private Entrada entrada;

    public ArrayList<DJ> listaDJ;
    public ArrayList<Evento> listaEventos;
    public ArrayList<Cliente> listaClientes;
    public ArrayList<Entrada> listaEntradas;

    public Cliente clienteSeleccionado;
    public DJ djSeleccionado;
    public Entrada entradaSeleccionada;
    public Evento eventoSeleccionado;

    public boolean nuevodj=false;
    public boolean nuevoCliente=false;
    public boolean nuevaEntrada=false;
    public boolean nuevoEvento=false;

    public VentanaController(Ventana ventana,VentanaModel model){
        this.ventana=ventana;
        this.model=model;

        inicializar();

        ventana.getlCliente().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                clienteSeleccionado = (Cliente) ventana.getlCliente().getSelectedValue();
                cargarClientes();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        ventana.getlDj().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                djSeleccionado=(DJ) ventana.getlDj().getSelectedValue();
                cargarDj();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        ventana.getlEntrada().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                entradaSeleccionada=(Entrada) ventana.lEntrada.getSelectedValue();
                cargarEntrada();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        ventana.getlEvento().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                eventoSeleccionado=(Evento) ventana.getlEvento().getSelectedValue();
                cargarEvento();
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });


        ventana.getBtNuevoDj().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listaDJ = new ArrayList<DJ>();
                activarCeldasDj();
                nuevodj = true;
            }
        });
        ventana.getBtGuardarDj().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarDJ();
                cancelarCeldasDj();
                rellenarComboDjEvento();
                nuevodj=false;
            }

        });
        ventana.getBtCancelarDj().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelarCeldasDj();
            }
        });
        ventana.getBtEliminarDj().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarDJ();
                listarDJ();
            }
        });
        ventana.getBtModificarDj().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                activarCeldasDj();
            }
        });
        ventana.btBuscarDj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<DJ> listaDJ=new ArrayList<DJ>();
                listaDJ=model.getBuscarDj(ventana.tfBuscarDj.getText());
                ventana.modeloListaDJ.removeAllElements();
                for(DJ dj:listaDJ){
                    ventana.modeloListaDJ.addElement(dj);
                }

            }
        });





        ventana.getBtNuevoEvento().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listaEventos=new ArrayList<Evento>();
                activarCeldasEvento();
                nuevoEvento=true;
            }


        });
        ventana.getBtCancelarEvento().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                activarCeldasEvento();
            }


        });
        ventana.getBtGuardarEvento().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarEvento();
                rellenarComboEventoEntrada();
                rellenarComboClienteEntrada();
                desactivarCeldasEvento();
                nuevoEvento=false;
            }
        });
        ventana.getBtEliminarEvento().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarEvento();
                listarEventos();
            }
        });
        ventana.getBtModificarEvento().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                activarCeldasEvento();
            }
        });



        ventana.getBtNuevoCliente().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listaClientes=new ArrayList<Cliente>();
                activarCeldasCliente();
                nuevoCliente=true;
            }

        });
        ventana.getBtGuardarCliente().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarCliente();
                rellenarComboClienteEntrada();
                rellenarComboEventoEntrada();
                desactivarCeldasCliente();
                nuevoCliente=false;
            }
        });
        ventana.getBtCancelarCliente().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelarCeldasCliente();
            }
        });
        ventana.getBtEliminarCliente().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarCliente();
                cancelarCeldasCliente();
                listarClientes();
            }
        });
        ventana.getBtModificarCliente().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                activarCeldasCliente();
            }
        });



        ventana.getBtNuevoEntrada().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listaEntradas=new ArrayList<Entrada>();
                activarCeldasEntrada();
                nuevaEntrada=true;
            }
        });
        ventana.getBtGuardarEntrada().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarEntrada();
                desactivarCeltasEntrada();
                nuevaEntrada=false;
            }


        });
        ventana.getBtCancelarentrada().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelarCeldasEntrada();
            }
        });
        ventana.getBtEliminarEntrada().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarEntrada();
                listarEntradas();
            }
        });
        ventana.getBtModificarEntrada().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                activarCeldasEntrada();
            }
        });

    }


    public void inicializar(){
        model.conectar();
        rellenarComboDjEvento();
        rellenarComboEventoDj();
        rellenarComboEventoCliente();
        rellenarComboClienteEntrada();
        rellenarComboEventoEntrada();
        rellenarComboClienteEntrada();
        rellenarComboClienteEvento();


        listarDJ();
        listarEventos();
        listarClientes();
        listarEntradas();


    }

    public void eliminarDJ(){
        dj = (DJ) ventana.lDj.getSelectedValue();
        model.eliminarDJ(dj);
        cancelarCeldasDj();
    }

    public void eliminarCliente(){
        cliente=(Cliente) ventana.lCliente.getSelectedValue();
        model.eliminarCliente(cliente);
        cancelarCeldasCliente();
    }

    public void eliminarEvento(){
        evento=(Evento) ventana.lEvento.getSelectedValue();
        model.eliminarEvento(evento);
        cancelarCeldasEvento();
    }

    public void eliminarEntrada(){
        entrada=(Entrada) ventana.lEntrada.getSelectedValue();
        model.eliminarEntrada(entrada);
    }

    private void guardarEvento() {
        if(nuevoEvento) {
            String nombreEvento = ventana.getTfNombreEvento().getText();
            Integer cantidadEventos = Integer.parseInt(ventana.getTfCantidadEventos().getText());
            String ciudadEventos = ventana.getTfCiudadEventos().getText();
            Date fecha_apertura = ventana.getJdcFechaAperturaEventos().getDate();
            DJ dj=(DJ) ventana.getCbDjEvento().getSelectedItem();
            Cliente client=(Cliente) ventana.getCbClienteEntrada().getSelectedItem();

            evento = new Evento();

            evento.setNombre(nombreEvento);
            evento.setCantidad_fiestas(cantidadEventos);
            evento.setCiudad(ciudadEventos);
            evento.setFecha_apertura(fecha_apertura);
            evento.getDj().add(dj);
            evento.getListaCliente().add(client);

            listaEventos.add(evento);
            model.guardarEvento(evento);
            listarEventos();
        }else{
            evento=(Evento) ventana.lEvento.getSelectedValue();

            String nombreEvento = ventana.getTfNombreEvento().getText();
            Integer cantidadEventos = Integer.parseInt(ventana.getTfCantidadEventos().getText());
            String ciudadEventos = ventana.getTfCiudadEventos().getText();
            Date fecha_apertura = ventana.getJdcFechaAperturaEventos().getDate();
            DJ dj=(DJ) ventana.getCbDjEvento().getSelectedItem();
            Cliente client=(Cliente) ventana.getCbClienteEntrada().getSelectedItem();


            evento.setNombre(nombreEvento);
            evento.setCantidad_fiestas(cantidadEventos);
            evento.setCiudad(ciudadEventos);
            evento.setFecha_apertura(fecha_apertura);
            evento.getDj().add(dj);
            evento.getListaCliente().add(client);

            model.modificarEvento(evento);
            listarEventos();
        }

    }

    private void guardarEntrada() {
        if(nuevaEntrada) {
            String codigo = ventana.getTfCodigoEntrada().getText();
            float precio = Float.parseFloat(ventana.getTfPrecioEntrada().getText());
            Date fecha = ventana.getJdcFechaCaducidadEntrada().getDate();

            entrada = new Entrada();
            entrada.setCodigo(codigo);
            entrada.setPrecio(precio);
            entrada.setFecha_caducidad(fecha);
            entrada.setCliente((Cliente) ventana.getCbClienteEntrada().getSelectedItem());
            entrada.setEvento((Evento) ventana.getCbEventoEntrada().getSelectedItem());

            listaEntradas.add(entrada);
            model.guardarEntrada(entrada);
            listarEntradas();
        }else{
            entrada=(Entrada) ventana.lEntrada.getSelectedValue();

            String codigo = ventana.getTfCodigoEntrada().getText();
            float precio = Float.parseFloat(ventana.getTfPrecioEntrada().getText());
            Date fecha = ventana.getJdcFechaCaducidadEntrada().getDate();

            entrada.setCodigo(codigo);
            entrada.setPrecio(precio);
            entrada.setFecha_caducidad(fecha);
            entrada.setCliente((Cliente) ventana.getCbClienteEntrada().getSelectedItem());
            entrada.setEvento((Evento) ventana.getCbEventoEntrada().getSelectedItem());

            model.modificarEntrada(entrada);
            listarEntradas();
        }

    }

    public void guardarCliente(){
        if(nuevoCliente) {
            listaClientes=new ArrayList<>();
            String nombre = ventana.getTfNombreCliente().getText();
            String apellidos = ventana.getTfDniCliente().getText();
            Date fecha_nacimiento = ventana.getJdcFechaNacimientoCliente().getDate();
            int numeroCliente = Integer.parseInt(ventana.getTfNumeroCliente().getText());
            Evento event = (Evento) ventana.getCbEventoCliente().getSelectedItem();


            cliente = new Cliente();

            cliente.setNombre(nombre);
            cliente.setDni(apellidos);
            cliente.setFecha_nacimiento(fecha_nacimiento);
            cliente.setNumero_cliente(numeroCliente);
            cliente.getEvento().add(event);
            listaClientes.add(cliente);
            model.guardarCliente(cliente);
            listarClientes();
        }else{
            cliente=(Cliente) ventana.lCliente.getSelectedValue();

            String nombre = ventana.getTfNombreCliente().getText();
            String apellidos = ventana.getTfDniCliente().getText();
            Date fecha_nacimiento = ventana.getJdcFechaNacimientoCliente().getDate();
            int numeroCliente = Integer.parseInt(ventana.getTfNumeroCliente().getText());
            Evento event = (Evento) ventana.getCbEventoCliente().getSelectedItem();



            cliente.setNombre(nombre);
            cliente.setDni(apellidos);
            cliente.setFecha_nacimiento(fecha_nacimiento);
            cliente.setNumero_cliente(numeroCliente);
            cliente.getEvento().add(event);

            model.modificarCliente(cliente);
            listarClientes();
        }



    }

    public void guardarDJ(){
            if(nuevodj) {
                listaDJ = new ArrayList<>();
                String nombre = ventana.getTfNombreDj().getText();
                Date fecha_contrato = ventana.getJdcFechaContrato().getDate();
                Float sueldo = Float.parseFloat(ventana.getTfSueldoDj().getText());
                Integer edad = Integer.parseInt(ventana.getTfEdadDj().getText());
                String categoria = (String) ventana.getCbCategoriaDj().getSelectedItem();
                Evento event = (Evento) ventana.getCbEventoDj().getSelectedItem();

                dj = new DJ();
                dj.setNombre(nombre);
                dj.setFecha_contrato(fecha_contrato);
                dj.setSueldo(sueldo);
                dj.setEdad(edad);
                dj.setCategoria(categoria);
                dj.getEvento().add(event);

                listaDJ.add(dj);
                model.guardarDJ(dj);
                listarDJ();
                desactivarCeldasDj();
            }else{
                dj = (DJ) ventana.lDj.getSelectedValue();
                String nombre = ventana.getTfNombreDj().getText();
                Date fecha_contrato = ventana.getJdcFechaContrato().getDate();
                Float sueldo = Float.parseFloat(ventana.getTfSueldoDj().getText());
                Integer edad = Integer.parseInt(ventana.getTfEdadDj().getText());
                String categoria = (String) ventana.getCbCategoriaDj().getSelectedItem();
                Evento event = (Evento) ventana.getCbEventoDj().getSelectedItem();



                dj.setNombre(nombre);
                dj.setFecha_contrato(fecha_contrato);
                dj.setSueldo(sueldo);
                dj.setEdad(edad);
                dj.setCategoria(categoria);
                dj.getEvento().add(event);

                model.modificarDJ(dj);
                listarDJ();
                desactivarCeldasDj();
            }

    }

    public void listarEventos(){
        List<Evento> listaEventos=model.getEventos();
        ventana.getModeloListaEventos().removeAllElements();
        for(Evento evento:listaEventos){
            ventana.getModeloListaEventos().addElement(evento);
        }
    }

    public void listarClientes(){
        List<Cliente> listaClientes= model.getClientes();
        ventana.getModeloListaCliente().removeAllElements();
        for(Cliente c:listaClientes){
            ventana.getModeloListaCliente().addElement(c);
        }
    }

    public void listarEntradas(){
        List<Entrada> listaEntradas=model.getEntradas();
        ventana.getModeloListaEntrada().removeAllElements();
        for(Entrada e:listaEntradas){
            ventana.getModeloListaEntrada().addElement(e);
        }
    }





    public void listarDJ(){
        List<DJ> listaDJS=model.getDjs();
        ventana.getModeloListaDJ().removeAllElements();
        for(DJ dj:listaDJS){
            ventana.getModeloListaDJ().addElement(dj);
        }
    }

    public void activarCeldasDj(){
        ventana.getTfNombreDj().setEnabled(true);
        ventana.getTfSueldoDj().setEnabled(true);
        ventana.getTfEdadDj().setEnabled(true);
        ventana.getJdcFechaContrato().setEnabled(true);
        ventana.getCbCategoriaDj().setEnabled(true);
        ventana.getCbEventoDj().setEnabled(true);

    }

    private void activarCeldasCliente() {
        ventana.getTfNombreCliente().setEnabled(true);
        ventana.getTfDniCliente().setEnabled(true);
        ventana.getTfNumeroCliente().setEnabled(true);
        ventana.getTfDniCliente().setEnabled(true);
        ventana.getCbEventoCliente().setEnabled(true);
        ventana.getJdcFechaNacimientoCliente().setEnabled(true);
    }

    private void activarCeldasEvento() {
        ventana.getTfNombreEvento().setEnabled(true);
        ventana.getTfCantidadEventos().setEnabled(true);
        ventana.getTfCiudadEventos().setEnabled(true);
        ventana.getJdcFechaAperturaEventos().setEnabled(true);
        ventana.getCbClienteEvento().setEnabled(true);
        ventana.getCbDjEvento().setEnabled(true);
    }

    private void activarCeldasEntrada(){
        ventana.getTfCodigoEntrada().setEnabled(true);
        ventana.getTfPrecioEntrada().setEnabled(true);
        ventana.getCbClienteEntrada().setEnabled(true);
        ventana.getCbEventoEntrada().setEnabled(true);
        ventana.getJdcFechaCaducidadEntrada().setEnabled(true);
    }





    public void desactivarCeldasDj(){
        ventana.getTfNombreDj().setEnabled(false);
        ventana.getTfSueldoDj().setEnabled(false);
        ventana.getTfEdadDj().setEnabled(false);
        ventana.getJdcFechaContrato().setEnabled(false);
        ventana.getCbCategoriaDj().setEnabled(false);
        ventana.getCbEventoDj().setEnabled(false);
    }

    private void desactivarCeldasCliente() {
        ventana.getTfNombreCliente().setEnabled(false);
        ventana.getTfDniCliente().setEnabled(false);
        ventana.getTfNumeroCliente().setEnabled(false);
        ventana.getTfDniCliente().setEnabled(false);
        ventana.getCbEventoCliente().setEnabled(false);
        ventana.getJdcFechaNacimientoCliente().setEnabled(false);
    }

    private void desactivarCeldasEvento() {
        ventana.getTfNombreEvento().setEnabled(false);
        ventana.getTfCantidadEventos().setEnabled(false);
        ventana.getTfCiudadEventos().setEnabled(false);
        ventana.getJdcFechaAperturaEventos().setEnabled(false);
        ventana.getCbClienteEvento().setEnabled(false);
        ventana.getCbDjEvento().setEnabled(false);
    }

    private void desactivarCeltasEntrada(){
        ventana.getTfCodigoEntrada().setEnabled(false);
        ventana.getTfPrecioEntrada().setEnabled(false);
        ventana.getCbClienteEntrada().setEnabled(false);
        ventana.getCbEventoEntrada().setEnabled(false);
        ventana.getJdcFechaCaducidadEntrada().setEnabled(false);
    }



    public void cancelarCeldasDj(){
        String palabra="";
        ventana.getTfNombreDj().setText(palabra);
        ventana.getTfEdadDj().setText(palabra);
        ventana.getTfSueldoDj().setText(palabra);
        ventana.getJdcFechaContrato().setDate(null);
    }

    public void cancelarCeldasCliente(){
        String palabra="";
        ventana.getTfNombreCliente().setText(palabra);
        ventana.getTfDniCliente().setText(palabra);
        ventana.getTfNumeroCliente().setText(palabra);
        ventana.getTfDniCliente().setText(palabra);
        ventana.getCbEventoCliente().setToolTipText(palabra);
        ventana.getJdcFechaNacimientoCliente().setDate(null);
    }

    public void cancelarCeldasEntrada(){
        String palabra="";
        ventana.getTfCodigoEntrada().setText(palabra);
        ventana.getTfPrecioEntrada().setText(palabra);
        ventana.getCbClienteEntrada().setToolTipText(palabra);
        ventana.getCbEventoEntrada().setToolTipText(palabra);
        ventana.getJdcFechaCaducidadEntrada().setDate(null);
    }

    public void cancelarCeldasEvento(){
        String palabra="";
        ventana.getTfNombreEvento().setText(palabra);
        ventana.getTfCantidadEventos().setText(palabra);
        ventana.getTfCiudadEventos().setText(palabra);
        ventana.getJdcFechaAperturaEventos().setDate(null);
        ventana.getCbClienteEvento().setToolTipText(palabra);
        ventana.getCbDjEvento().setToolTipText(palabra);
    }

    public void rellenarComboDjEvento(){
        List<DJ> listaDj=new ArrayList<>();
            listaDj=model.getDjs();
            ventana.getCbDjEvento().removeAllItems();
            for(DJ dj:listaDj){
                ventana.getCbDjEvento().addItem(dj);
            }
    }


    public void rellenarComboClienteEvento(){
        List<Cliente> listaClientes=new ArrayList<>();
        listaClientes=model.getClientes();
        ventana.getCbClienteEvento().removeAllItems();
        for(Cliente cliente:listaClientes){
            ventana.getCbClienteEvento().addItem(cliente);
        }
    }

    public void rellenarComboEventoDj(){
        List<Evento> listaEventos=new ArrayList<>();
        listaEventos=model.getEventos();
        ventana.getCbEventoDj().removeAllItems();
        for(Evento e:listaEventos){
            ventana.getCbEventoDj().addItem(e);
        }
    }

    public void rellenarComboEventoCliente(){
        List<Evento> listaEventos=new ArrayList<>();
        listaEventos=model.getEventos();
        ventana.getCbEventoCliente().removeAllItems();
        for(Evento e:listaEventos){
            ventana.getCbEventoCliente().addItem(e);
        }
    }

    public void rellenarComboClienteEntrada(){
        List<Cliente> listaClientes=new ArrayList<>();
        listaClientes=model.getClientes();
        ventana.getCbClienteEntrada().removeAllItems();
        for(Cliente c:listaClientes){
            ventana.getCbClienteEntrada().addItem(c);
        }
    }

    public void rellenarComboEventoEntrada(){
        List<Evento> listaEventos=new ArrayList<>();
        listaEventos=model.getEventos();
        ventana.getCbEventoEntrada().removeAllItems();
        for(Evento e:listaEventos){
            ventana.getCbEventoEntrada().addItem(e);
        }
    }




    public void cargarClientes(){
        cliente=new Cliente();

        ventana.tfNombreCliente.setText(clienteSeleccionado.getNombre());
        ventana.tfDniCliente.setText(clienteSeleccionado.getDni());
        ventana.tfNumeroCliente.setText(String.valueOf(clienteSeleccionado.getNumero_cliente()));
        ventana.cbEventoCliente.setSelectedItem(String.valueOf(clienteSeleccionado.getEvento()));
        ventana.jdcFechaNacimientoCliente.setDate(clienteSeleccionado.getFecha_nacimiento());

    }

    public void cargarDj(){
        dj=new DJ();
        ventana.tfNombreDj.setText(djSeleccionado.getNombre());
        ventana.cbCategoriaDj.setToolTipText(String.valueOf(djSeleccionado.getCategoria()));
        ventana.tfSueldoDj.setText(String.valueOf(djSeleccionado.getSueldo()));
        ventana.tfEdadDj.setText(String.valueOf(djSeleccionado.getEdad()));
        ventana.jdcFechaContrato.setDate(djSeleccionado.getFecha_contrato());
        ventana.cbEventoDj.setSelectedItem(String.valueOf(djSeleccionado.getEvento()));
    }

    public void cargarEvento(){
        ventana.tfNombreEvento.setText(eventoSeleccionado.getNombre());
        ventana.tfCantidadEventos.setText(String.valueOf(eventoSeleccionado.getCantidad_fiestas()));
        ventana.tfCiudadEventos.setText(eventoSeleccionado.getCiudad());
        ventana.jdcFechaAperturaEventos.setDate(eventoSeleccionado.getFecha_apertura());
        ventana.cbDjEvento.setToolTipText(String.valueOf(eventoSeleccionado.getDj()));
        ventana.cbClienteEvento.setSelectedItem(String.valueOf(eventoSeleccionado.getListaCliente()));
    }

    public void cargarEntrada(){
        ventana.tfCodigoEntrada.setText(entradaSeleccionada.getCodigo());
        ventana.tfPrecioEntrada.setText(String.valueOf(entradaSeleccionada.getPrecio()));
        ventana.jdcFechaCaducidadEntrada.setDate(entradaSeleccionada.getFecha_caducidad());
        ventana.cbClienteEntrada.setToolTipText(String.valueOf(entradaSeleccionada.getCliente()));
        ventana.cbEventoEntrada.setSelectedItem(String.valueOf(entradaSeleccionada.getEvento()));
    }
}
