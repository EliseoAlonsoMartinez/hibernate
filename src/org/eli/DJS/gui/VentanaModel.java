package org.eli.DJS.gui;

import org.eli.DJS.Util.HibernateUtil;
import org.eli.DJS.base.Cliente;
import org.eli.DJS.base.DJ;
import org.eli.DJS.base.Entrada;
import org.eli.DJS.base.Evento;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Eliseo on 06/02/2016.
 */
public class VentanaModel {



    public void conectar(){
        try {
            HibernateUtil.buildSessionFactory();
            HibernateUtil.openSession();

        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public void guardarDJ(DJ dj){
        Session sesion= HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(dj);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void guardarEntrada(Entrada entrada){
        Session sesion= HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(entrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void guardarEvento(Evento evento){
        Session sesion=HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(evento);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void guardarCliente(Cliente cliente){
        Session sesion=HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.save(cliente);
        sesion.getTransaction().commit();
        sesion.close();
    }







    public void modificarDJ(DJ dj){
        Session sesion= HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(dj);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarEntrada(Entrada entrada){
        Session sesion= HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(entrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarEvento(Evento evento){
        Session sesion=HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(evento);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarCliente(Cliente cliente){
        Session sesion=HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(cliente);
        sesion.getTransaction().commit();
        sesion.close();
    }






    public void eliminarDJ(DJ dj){
        Session sesion= HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(dj);
        sesion.getTransaction().commit();

    }

    public void eliminarEntrada(Entrada entrada){
        Session sesion= HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(entrada);
        sesion.getTransaction().commit();

    }

    public void eliminarEvento(Evento evento){
        Session sesion=HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(evento);
        sesion.getTransaction().commit();

    }

    public void eliminarCliente(Cliente cliente){
        Session sesion=HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(cliente);
        sesion.getTransaction().commit();
    }









    public ArrayList<Cliente> getClientes(){
        Query query=HibernateUtil.getCurrentSession().createQuery("FROM Cliente ");
        ArrayList<Cliente> listaClientes=(ArrayList<Cliente>) query.list();
        return listaClientes;
    }

    public ArrayList<Entrada> getEntradas(){
        Query query=HibernateUtil.getCurrentSession().createQuery("FROM Entrada ");
        ArrayList<Entrada> listaEntradas=(ArrayList<Entrada>) query.list();
        return listaEntradas;
    }

    public ArrayList<DJ> getDjs() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM DJ");
        ArrayList<DJ> djs = (ArrayList<DJ>) query.list();

        return djs;
    }

    public List<DJ> getBuscarDj(String nombre){
        Query query=HibernateUtil.getCurrentSession().createQuery("FROM DJ  WHERE nombre= :nombre");
        query.setParameter("nombre",nombre);
        ArrayList<DJ> listaDJS=(ArrayList<DJ>) query.list();

        return listaDJS;
    }

    public ArrayList<Evento> getEventos() {

        Query query = HibernateUtil.getCurrentSession().
                createQuery("FROM Evento");
        ArrayList<Evento> evento = (ArrayList<Evento>) query.list();

        return evento;
    }





}
