package org.eli.DJS.gui;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;

/**
 * Created by Eliseo on 06/02/2016.
 */
public class Ventana {
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    public JTextField tfNombreDj;
    public JTextField tfSueldoDj;
    public JComboBox cbCategoriaDj;
    public JTextField tfEdadDj;
    public JList lDj;
    private JButton btNuevoDj;
    private JButton btGuardarDj;
    private JButton btModificarDj;
    private JButton btEliminarDj;
    public JDateChooser jdcFechaContrato;
    public JTextField tfNombreCliente;
    public JTextField tfDniCliente;
    private JButton btCancelarDj;
    public JList lCliente;
    public JTextField tfNumeroCliente;
    private JButton btNuevoCliente;
    private JButton btGuardarCliente;
    private JButton btModificarCliente;
    private JButton btCancelarCliente;
    private JButton btEliminarCliente;
    public JDateChooser jdcFechaNacimientoCliente;
    public JTextField tfNombreEvento;
    public JTextField tfCantidadEventos;
    public JTextField tfCiudadEventos;
    public JList lEvento;
    private JButton btNuevoEvento;
    private JButton btGuardarEvento;
    private JButton btEliminarEvento;
    private JButton btModificarEvento;
    private JButton btCancelarEvento;
    public JTextField tfCodigoEntrada;
    public JTextField tfPrecioEntrada;
    public JList lEntrada;
    private JButton btNuevoEntrada;
    private JButton btGuardarEntrada;
    private JButton btModificarEntrada;
    private JButton btCancelarentrada;
    private JButton btEliminarEntrada;
    public JComboBox cbEventoDj;
    public JComboBox cbDjEvento;
    public JComboBox cbClienteEntrada;
    public JComboBox cbEventoEntrada;
    public JComboBox cbClienteEvento;
    public JComboBox cbEventoCliente;
    public JDateChooser jdcFechaAperturaEventos;
    public JDateChooser jdcFechaCaducidadEntrada;
    public JTextField tfBuscarDj;
    public JButton btBuscarDj;


    public DefaultListModel modeloListaDJ;
    private DefaultListModel modeloListaCliente;
    private DefaultListModel modeloListaEventos;
    private DefaultListModel modeloListaEntrada;


    public Ventana(){
        JFrame frame=new JFrame();
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        inicializar();
    }
    public void inicializar(){


        modeloListaCliente=new DefaultListModel();
        modeloListaDJ=new DefaultListModel();
        modeloListaEntrada=new DefaultListModel();
        modeloListaEventos=new DefaultListModel();

        lCliente.setModel(modeloListaCliente);
        lDj.setModel(modeloListaDJ);
        lEvento.setModel(modeloListaEventos);
        lEntrada.setModel(modeloListaEntrada);
    }

    public JComboBox getCbEventoCliente() {
        return cbEventoCliente;
    }

    public void setCbEventoCliente(JComboBox cbEventoCliente) {
        this.cbEventoCliente = cbEventoCliente;
    }

    public JComboBox getCbClienteEvento() {
        return cbClienteEvento;
    }

    public void setCbClienteEvento(JComboBox cbClienteEvento) {
        this.cbClienteEvento = cbClienteEvento;
    }

    public JDateChooser getJdcFechaAperturaEventos() {
        return jdcFechaAperturaEventos;
    }

    public void setJdcFechaAperturaEventos(JDateChooser jdcFechaAperturaEventos) {
        this.jdcFechaAperturaEventos = jdcFechaAperturaEventos;
    }

    public JTabbedPane getTabbedPane1() {
        return tabbedPane1;
    }

    public void setTabbedPane1(JTabbedPane tabbedPane1) {
        this.tabbedPane1 = tabbedPane1;
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }

    public JTextField getTfNombreDj() {
        return tfNombreDj;
    }

    public void setTfNombreDj(JTextField tfNombreDj) {
        this.tfNombreDj = tfNombreDj;
    }

    public JTextField getTfSueldoDj() {
        return tfSueldoDj;
    }

    public void setTfSueldoDj(JTextField tfSueldoDj) {
        this.tfSueldoDj = tfSueldoDj;
    }

    public JComboBox getCbCategoriaDj() {
        return cbCategoriaDj;
    }

    public void setCbCategoriaDj(JComboBox cbCategoriaDj) {
        this.cbCategoriaDj = cbCategoriaDj;
    }

    public JTextField getTfEdadDj() {
        return tfEdadDj;
    }

    public void setTfEdadDj(JTextField tfEdadDj) {
        this.tfEdadDj = tfEdadDj;
    }

    public JList getlDj() {
        return lDj;
    }

    public void setlDj(JList lDj) {
        this.lDj = lDj;
    }

    public JButton getBtNuevoDj() {
        return btNuevoDj;
    }

    public void setBtNuevoDj(JButton btNuevoDj) {
        this.btNuevoDj = btNuevoDj;
    }

    public JButton getBtGuardarDj() {
        return btGuardarDj;
    }

    public void setBtGuardarDj(JButton btGuardarDj) {
        this.btGuardarDj = btGuardarDj;
    }

    public JButton getBtModificarDj() {
        return btModificarDj;
    }

    public void setBtModificarDj(JButton btModificarDj) {
        this.btModificarDj = btModificarDj;
    }

    public JButton getBtEliminarDj() {
        return btEliminarDj;
    }

    public void setBtEliminarDj(JButton btEliminarDj) {
        this.btEliminarDj = btEliminarDj;
    }

    public JDateChooser getJdcFechaContrato() {
        return jdcFechaContrato;
    }

    public void setJdcFechaContrato(JDateChooser jdcFechaContrato) {
        this.jdcFechaContrato = jdcFechaContrato;
    }

    public JTextField getTfNombreCliente() {
        return tfNombreCliente;
    }

    public void setTfNombreCliente(JTextField tfNombreCliente) {
        this.tfNombreCliente = tfNombreCliente;
    }

    public JTextField getTfDniCliente() {
        return tfDniCliente;
    }

    public void setTfDniCliente(JTextField tfDniCliente) {
        this.tfDniCliente = tfDniCliente;
    }

    public JButton getBtCancelarDj() {
        return btCancelarDj;
    }

    public void setBtCancelarDj(JButton btCancelarDj) {
        this.btCancelarDj = btCancelarDj;
    }

    public JList getlCliente() {
        return lCliente;
    }

    public void setlCliente(JList lCliente) {
        this.lCliente = lCliente;
    }

    public JTextField getTfNumeroCliente() {
        return tfNumeroCliente;
    }

    public void setTfNumeroCliente(JTextField tfNumeroCliente) {
        this.tfNumeroCliente = tfNumeroCliente;
    }

    public JButton getBtNuevoCliente() {
        return btNuevoCliente;
    }

    public void setBtNuevoCliente(JButton btNuevoCliente) {
        this.btNuevoCliente = btNuevoCliente;
    }

    public JButton getBtGuardarCliente() {
        return btGuardarCliente;
    }

    public void setBtGuardarCliente(JButton btGuardarCliente) {
        this.btGuardarCliente = btGuardarCliente;
    }

    public JButton getBtModificarCliente() {
        return btModificarCliente;
    }

    public void setBtModificarCliente(JButton btModificarCliente) {
        this.btModificarCliente = btModificarCliente;
    }

    public JButton getBtCancelarCliente() {
        return btCancelarCliente;
    }

    public void setBtCancelarCliente(JButton btCancelarCliente) {
        this.btCancelarCliente = btCancelarCliente;
    }

    public JButton getBtEliminarCliente() {
        return btEliminarCliente;
    }

    public void setBtEliminarCliente(JButton btEliminarCliente) {
        this.btEliminarCliente = btEliminarCliente;
    }

    public JDateChooser getJdcFechaNacimientoCliente() {
        return jdcFechaNacimientoCliente;
    }

    public void setJdcFechaNacimientoCliente(JDateChooser jdcFechaNacimientoCliente) {
        this.jdcFechaNacimientoCliente = jdcFechaNacimientoCliente;
    }

    public JTextField getTfNombreEvento() {
        return tfNombreEvento;
    }

    public void setTfNombreEvento(JTextField tfNombreEvento) {
        this.tfNombreEvento = tfNombreEvento;
    }

    public JTextField getTfCantidadEventos() {
        return tfCantidadEventos;
    }

    public void setTfCantidadEventos(JTextField tfCantidadEventos) {
        this.tfCantidadEventos = tfCantidadEventos;
    }

    public JTextField getTfCiudadEventos() {
        return tfCiudadEventos;
    }

    public void setTfCiudadEventos(JTextField tfCiudadEventos) {
        this.tfCiudadEventos = tfCiudadEventos;
    }

    public JList getlEvento() {
        return lEvento;
    }

    public void setlEvento(JList lEvento) {
        this.lEvento = lEvento;
    }

    public JButton getBtNuevoEvento() {
        return btNuevoEvento;
    }

    public void setBtNuevoEvento(JButton btNuevoEvento) {
        this.btNuevoEvento = btNuevoEvento;
    }

    public JButton getBtGuardarEvento() {
        return btGuardarEvento;
    }

    public void setBtGuardarEvento(JButton btGuardarEvento) {
        this.btGuardarEvento = btGuardarEvento;
    }

    public JButton getBtEliminarEvento() {
        return btEliminarEvento;
    }

    public void setBtEliminarEvento(JButton btEliminarEvento) {
        this.btEliminarEvento = btEliminarEvento;
    }

    public JButton getBtModificarEvento() {
        return btModificarEvento;
    }

    public void setBtModificarEvento(JButton btModificarEvento) {
        this.btModificarEvento = btModificarEvento;
    }

    public JButton getBtCancelarEvento() {
        return btCancelarEvento;
    }

    public void setBtCancelarEvento(JButton btCancelarEvento) {
        this.btCancelarEvento = btCancelarEvento;
    }

    public JTextField getTfCodigoEntrada() {
        return tfCodigoEntrada;
    }

    public void setTfCodigoEntrada(JTextField tfCodigoEntrada) {
        this.tfCodigoEntrada = tfCodigoEntrada;
    }

    public JTextField getTfPrecioEntrada() {
        return tfPrecioEntrada;
    }

    public void setTfPrecioEntrada(JTextField tfPrecioEntrada) {
        this.tfPrecioEntrada = tfPrecioEntrada;
    }

    public JList getlEntrada() {
        return lEntrada;
    }

    public void setlEntrada(JList lEntrada) {
        this.lEntrada = lEntrada;
    }

    public JButton getBtNuevoEntrada() {
        return btNuevoEntrada;
    }

    public void setBtNuevoEntrada(JButton btNuevoEntrada) {
        this.btNuevoEntrada = btNuevoEntrada;
    }

    public JButton getBtGuardarEntrada() {
        return btGuardarEntrada;
    }

    public void setBtGuardarEntrada(JButton btGuardarEntrada) {
        this.btGuardarEntrada = btGuardarEntrada;
    }

    public JButton getBtModificarEntrada() {
        return btModificarEntrada;
    }

    public void setBtModificarEntrada(JButton btModificarEntrada) {
        this.btModificarEntrada = btModificarEntrada;
    }

    public JButton getBtCancelarentrada() {
        return btCancelarentrada;
    }

    public void setBtCancelarentrada(JButton btCancelarentrada) {
        this.btCancelarentrada = btCancelarentrada;
    }

    public JButton getBtEliminarEntrada() {
        return btEliminarEntrada;
    }

    public void setBtEliminarEntrada(JButton btEliminarEntrada) {
        this.btEliminarEntrada = btEliminarEntrada;
    }

    public JComboBox getCbEventoDj() {
        return cbEventoDj;
    }

    public void setCbEventoDj(JComboBox cbEventoDj) {
        this.cbEventoDj = cbEventoDj;
    }

    public JComboBox getCbDjEvento() {
        return cbDjEvento;
    }

    public void setCbDjEvento(JComboBox cbDjEvento) {
        this.cbDjEvento = cbDjEvento;
    }

    public JComboBox getCbClienteEntrada() {
        return cbClienteEntrada;
    }

    public void setCbClienteEntrada(JComboBox cbClienteEntrada) {
        this.cbClienteEntrada = cbClienteEntrada;
    }

    public JComboBox getCbEventoEntrada() {
        return cbEventoEntrada;
    }

    public void setCbEventoEntrada(JComboBox cbEventoEntrada) {
        this.cbEventoEntrada = cbEventoEntrada;
    }

    public DefaultListModel getModeloListaDJ() {
        return modeloListaDJ;
    }

    public void setModeloListaDJ(DefaultListModel modeloListaDJ) {
        this.modeloListaDJ = modeloListaDJ;
    }

    public DefaultListModel getModeloListaCliente() {
        return modeloListaCliente;
    }

    public void setModeloListaCliente(DefaultListModel modeloListaCliente) {
        this.modeloListaCliente = modeloListaCliente;
    }

    public DefaultListModel getModeloListaEventos() {
        return modeloListaEventos;
    }

    public void setModeloListaEventos(DefaultListModel modeloListaEventos) {
        this.modeloListaEventos = modeloListaEventos;
    }

    public DefaultListModel getModeloListaEntrada() {
        return modeloListaEntrada;
    }

    public void setModeloListaEntrada(DefaultListModel modeloListaEntrada) {
        this.modeloListaEntrada = modeloListaEntrada;
    }

    public JDateChooser getJdcFechaCaducidadEntrada() {
        return jdcFechaCaducidadEntrada;
    }

    public void setJdcFechaCaducidadEntrada(JDateChooser jdcFechaCaducidadEntrada) {
        this.jdcFechaCaducidadEntrada = jdcFechaCaducidadEntrada;
    }
}
