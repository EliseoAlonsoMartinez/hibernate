package org.eli.DJS;

import org.eli.DJS.gui.Ventana;
import org.eli.DJS.gui.VentanaController;
import org.eli.DJS.gui.VentanaModel;

/**
 * Created by Eliseo on 06/02/2016.
 */
public class Principal {

    public static void main(String []args){
        Ventana ventana=new Ventana();
        VentanaModel vent=new VentanaModel();
        VentanaController ven=new VentanaController(ventana,vent);
    }
}
